"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

BREV_TIMES = [(200, 15, 34, 200), (400, 15, 32, 200), (600, 15, 30, 200), (1000, 11.428, 28, 400), (1300, 13.333, 26, 300)]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    time = 0
    dist_left = control_dist_km
    for brev_time in BREV_TIMES:
        brev_category, min_speed, max_speed, max_section = brev_time
        
        if control_dist_km > brev_category + 10:
            time = time + (max_section/max_speed)
            dist_left = dist_left - max_section
        else:
            time = time + (dist_left/max_speed)
            dist_left = 0

        if brev_category == brevet_dist_km:
            break

    arr_time = arrow.get(brevet_start_time)

    dhours = round(time//1)
    dminutes = round((time%1)*60)

    arr_time = arr_time.shift(hours=dhours)
    arr_time = arr_time.shift(minutes=dminutes)

    arr_time.to('US/Pacific')
    #print(brev_category, min_speed, max_speed)
    #print("Open after {}:{}".format(dhours, dminutes))
    #print(time)
    #print(brevet_start_time)
    #print(arr_time)
    #print("\n")

    #return arrow.now().isoformat()
    return arr_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    time = 0
    dist_left = control_dist_km
    for brev_time in BREV_TIMES:
        brev_category, min_speed, max_speed, max_section = brev_time
        
        if control_dist_km > brev_category + 10:
            time = time + (max_section/min_speed)
            dist_left = dist_left - max_section
        else:
            time = time + (dist_left/min_speed)
            dist_left = 0

        if (brev_category == brevet_dist_km):
            break

    arr_time = arrow.get(brevet_start_time)

    dhours = round(time//1)
    dminutes = round((time%1)*60)

    arr_time = arr_time.shift(hours=+dhours)
    arr_time = arr_time.shift(minutes=+dminutes)
    
    arr_time.to('US/Pacific')
    #print(brev_category, min_speed, max_speed)
    #print("Close after {}:{}".format(dhours, dminutes))
    #print(brevet_start_time)
    #print(arr_time)
    #print("\n")
        
    #return arrow.now().isoformat()
    return arr_time.isoformat()

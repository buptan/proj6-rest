import os
import flask
from flask import redirect, url_for, request, render_template, jsonify
from flask_restful import Resource, Api
import arrow
from pymongo import MongoClient
import acp_times

app = flask.Flask(__name__)
api = Api(app)

#client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#mongodb_url = os.environ.get('MONGO_URL')
#client = MongoClient(mongodb_url, 27017)
#client = MongoClient('mongodb://mongodb:27017')
client = MongoClient('172.28.1.3', 27017)
db = client.tododb
edb = client.entries

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell',
            'Windozzee',
            'Yet another laptop!',
            'Yet yet another laptop!',
            'Test']
        }

api.add_resource(Laptop, '/example')

class ListAllJSON(Resource):
    def get(self):
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        data = [0 for i in range(j)]
        i = 0
        for entry in entries:
            data[i] = "km: {}   open_time: {}   close_time: {}".format(entry['km'], entry['open_time'], entry['close_time'])
            i = i + 1

        return { 'control_points': data }

api.add_resource(ListAllJSON, '/listAll', '/listAll/json')

class ListOpenJSON(Resource):
    def get(self):
        if 'top' in request.args:
            top = int(request.args['top'])
        else:
            top = 0
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        if top == 0:
            data = [0 for i in range(j)]
        else:
            data = [0 for i in range(top)]
        i = 0
        for entry in entries:
            data[i] = "km: {}   open_time: {}".format(entry['km'], entry['open_time'])
            i = i + 1
            if top != 0:
                if top == i:
                    break

        return { 'control_points': data }

api.add_resource(ListOpenJSON, '/listOpenOnly/', '/listOpenOnly/json')

class ListCloseJSON(Resource):
    def get(self):
        if 'top' in request.args:
            top = int(request.args['top'])
        else:
            top = 0
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        if top == 0:
            data = [0 for i in range(j)]
        else:
            data = [0 for i in range(top)]
        i = 0
        for entry in entries:
            data[i] = "km: {}   close_time: {}".format(entry['km'], entry['close_time'])
            i = i + 1
            if top != 0:
                if top == i:
                    break

        return { 'control_points': data }

api.add_resource(ListCloseJSON, '/listCloseOnly', '/listCloseOnly/json')

class ListAllCSV(Resource):
    def get(self):
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        data = [0 for i in range(j)]
        i = 0
        for entry in entries:
            item = "{}, {}, {}".format(entry['km'], entry['open_time'], entry['close_time'])
            data[i] = item
            i = i + 1

        return { 'control_points': data }

api.add_resource(ListAllCSV, '/listAll/csv')

class ListOpenCSV(Resource):
    def get(self):
        if 'top' in request.args:
            top = int(request.args['top'])
        else:
            top = 0
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        if top == 0:
            data = [0 for i in range(j)]
        else:
            data = [0 for i in range(top)]
        i = 0
        for entry in entries:
            item = "{}, {}".format(entry['km'], entry['open_time'])
            data[i] = item
            i = i + 1
            if top != 0:
                if top == i:
                    break

        return { 'control_points': data }

api.add_resource(ListOpenCSV, '/listOpenOnly/csv')

class ListCloseCSV(Resource):
    def get(self):
        if 'top' in request.args:
            top = int(request.args['top'])
        else:
            top = 0
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        if top == 0:
            data = [0 for i in range(j)]
        else:
            data = [0 for i in range(top)]
        i = 0
        for entry in entries:
            item = "{}, {}".format(entry['km'], entry['close_time'])
            data[i] = item
            i = i + 1
            if top != 0:
                if top == i:
                    break

        return { 'control_points': data }

api.add_resource(ListCloseCSV, '/listCloseOnly/csv')

@app.route('/display')
def display():
    _entries = edb.entries.find()
    entries = [entry for entry in _entries]

    return render_template('display.html', entries=entries)

@app.route('/clear_edb')
def clear_edb():
    edb.entries.remove({})
    return render_template('clear_edb.html')

@app.route("/")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open and close times from km,
    using rules described at https://rusa.org/octime_alg.html.
    Expects 4 URL-encoded arguments, the number of km,
    the brevet category, the begin date and begin time.
    """
    app.logger.debug("got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_cat = request.args.get('brevet_dist', type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')

    #app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    add = [0,0,0]
    sect = 0
    ct = 0
    for char in begin_date:
        if char == '-':
            if ct == 1:
                add[sect] = 1
            ct = 0
            sect = sect + 1
        else:
            ct = ct + 1
    d_string = ""
    sect = 0
    for char in begin_date:
        if char ==  '-':
            sect = sect + 1
        elif add[sect] == 1:
            d_string = d_string + '0'
        d_string = d_string + char

    use_time = arrow.get(d_string + ' ' + begin_time, 'DD-MM-YYYY HH:mm')
    use_time.replace(tzinfo='US/Pacific')

    open_time = acp_times.open_time(km, brevet_cat, use_time)
    close_time = acp_times.close_time(km, brevet_cat, use_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/_handle_submit', methods=['POST','GET'])
def _handle_submit():
    data = request.get_json()
    app.logger.debug("handling submit button press")
    app.logger.debug(data)
    for entry in data:
        if entry[1] != "":
            entry_doc = {
                'km': entry[1],
                'open_time': entry[3],
                'close_time': entry[4]
            }
            #app.logger.debug(entry_doc)
            edb.entries.insert_one(entry_doc)
    result = ""
    return result


@app.route('/new')
def new():
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
